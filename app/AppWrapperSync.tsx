import { AppProvider, UserProvider } from "@realm/react";
import { SafeAreaView, StyleSheet } from "react-native";

import { AppSync } from "./AppSync";
import { LoginScreen } from "./components/LoginScreen";
import React from "react";
import { TaskRealmContext } from "./models";
import colors from "./styles/colors";

export const AppWrapperSync: React.FC<{
  appId: string;
}> = ({ appId }) => {
  const { RealmProvider } = TaskRealmContext;

  // If we are logged in, add the sync configuration the the RealmProvider and render the app
  return (
    <SafeAreaView style={styles.screen}>
      <AppProvider id={appId}>
        <UserProvider fallback={LoginScreen}>
          <RealmProvider sync={{ flexible: true }}>
            <AppSync />
          </RealmProvider>
        </UserProvider>
      </AppProvider>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: colors.darkBlue,
  },
});

export default AppWrapperSync;
