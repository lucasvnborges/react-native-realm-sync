import { Task } from './Task';
import { createRealmContext } from '@realm/react';

export const TaskRealmContext = createRealmContext({
  schema: [Task],
});
