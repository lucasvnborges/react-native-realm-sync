import { StyleSheet, Text, View } from "react-native";

import React from "react";

export const IntroText = () => {
  return (
    <View style={styles.content}>
      <Text style={styles.title}>
        Bem-vindo ao aplicativo Realm React-Native Task
      </Text>
      <Text style={styles.paragraph}>
        Comece adicionando uma tarefa usando o formulário na parte inferior da
        tela para ver como eles são criados no Realm. Você também pode alternar
        o status da tarefa ou remover da lista.
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 8,
    justifyContent: "center",
  },
  title: {
    fontSize: 18,
    color: "#333",
    fontWeight: "500",
    marginVertical: 10,
    textAlign: "center",
  },
  paragraph: {
    fontSize: 16,
    color: "#333",
    fontWeight: "400",
    marginVertical: 10,
    textAlign: "center",
  },
});
