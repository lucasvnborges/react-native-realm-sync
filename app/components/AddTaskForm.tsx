import { Pressable, StyleSheet, Text, TextInput, View } from "react-native";
import React, { useState } from "react";

import { buttonStyles } from "../styles/button";
import colors from "../styles/colors";

type AddTaskFormProps = {
  onSubmit: (description: string) => void;
};

export const AddTaskForm: React.FC<AddTaskFormProps> = ({ onSubmit }) => {
  const [description, setDescription] = useState("");

  const handleSubmit = () => {
    onSubmit(description);
    setDescription("");
  };

  return (
    <View style={styles.form}>
      <TextInput
        value={description}
        autoCorrect={false}
        autoCapitalize="none"
        style={styles.textInput}
        onChangeText={setDescription}
        placeholder="Descrição da tarefa"
      />
      <Pressable onPress={handleSubmit} style={styles.submit}>
        <Text style={styles.icon}>＋</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  form: {
    height: 64,
    marginBottom: 20,
    borderTopWidth: 2,
    paddingVertical: 12,
    flexDirection: "row",
    alignItems: "center",
    borderTopColor: "#f5f5f5",
  },
  textInput: {
    flex: 1,
    height: 50,
    fontSize: 17,
    borderRadius: 5,
    paddingHorizontal: 16,
    backgroundColor: colors.white,
  },
  submit: {
    ...buttonStyles.button,
    width: 50,
    height: "100%",
    paddingHorizontal: 0,
    paddingVertical: 0,
    marginLeft: 20,
    marginRight: 0,
  },
  icon: {
    ...buttonStyles.text,
  },
});
