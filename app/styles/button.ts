import { StyleSheet } from 'react-native';
import colors from './colors';

export const buttonStyles: StyleSheet.NamedStyles<any> = {
  button: {
    borderRadius: 12,
    paddingVertical: 10,
    marginHorizontal: 10,
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.purple,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    textAlign: 'center',
    color: colors.white,
  },
};
